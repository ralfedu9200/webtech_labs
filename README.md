
# Chat application - final project

This chat application is the final project of the Webtech course that we had during this semester at ECE Paris

## Usage


* Clone this repository, from your local machine:
  ```
  git clone https://bitbucket.org/ralfedu9200/webtech_labs.git webtech
  cd webtech
  ```
* Install [Go](https://golang.org/) and [Dex](https://dexidp.io/docs/getting-started/). For example, on Ubuntu, from your project root directory:   
  ```
  # Install Go
  apt install golang-go
  # Download Dex
  git clone https://github.com/dexidp/dex.git
  # Build Dex
  cd dex
  make
  make examples
  ```
  Note, the provided `.gitignore` file ignore the `dex` folder.
* Make a copy of the Dex configuration to `./dex-config/config-private.yaml`, the project is configured to Git ignore this path:
  ```bash
  cp -rp ./dex-config/config.yaml ./dex-config/config-private.yaml
  ```
* Register your GitHub application, get the clientID and clientSecret from GitHub and report them to your Dex configuration. Modify the provided `./dex-config/config-private.yaml` configuration to look like:
  ```yaml
  - type: github
    id: github
    name: GitHub
    config:
      clientID: xxxx98f1c26493dbxxxx
      clientSecret: xxxxxxxxx80e139441b637796b128d8xxxxxxxxx
      redirectURI: http://127.0.0.1:5556/dex/callback
  ```
* Inside `./dex-config/config-private.yaml`, the frond-end application is already registered and CORS is activated. Now that Dex is built and configured, your can start the Dex server:
  ```yaml
  cd dex
  bin/dex serve ../dex-config/config-private.yaml
  ```
* Start the back-end
  ```bash
  cd back-end
  # Install dependencies (use yarn or npm)
  yarn install
  # Optional, fill the database with initial data
  bin/init
  # Start the back-end
  bin/start
  ```
* Start the front-end
  ```bash
  cd front-end
  # Install dependencies (use yarn or npm)
  yarn install
  # Start the front-end
  yarn start
  ```

## Authors

**Anthony Dubost** - anthony.dubost@edu.ece.fr
**Jihad Abi Chacra** - jihad.abi-chacra@edu.ece.fr

## Tasks

Project management

* Naming convention   
  We respected the **naming convention** both for file name and for variables, functions...
* Project structure   
  We followed the same **project structure** as seen in class, using react *functions and hooks instead of classes*. We created *new routes in the back-end* to satisfy the needs of the application, for example to *edit or remove a message*.
* Code quality   
  We tried to *produce some proper code* with as *less empty spaces* as possible and using *relevant variable names* to *faciliate reading* our code.
* Design, UX   
  We did our best to try and make the application as **user-friendly** and **good-looking** as possible.
  Both our *light* and *dark* themes are created with care to provide **feedback** to the user (if you hover an *interactive element* on the app such as an icon the background color will change accordingly).
* Git and DevOps   
  We used **Git** (bitbucket) a lot to share our progress during the development of the app. We tried to *name our commits* with care to *facilitate navigating* through our source tree. 

Application development

* Welcome screens   
  We made a **special effort** trying to have the **application look as good as possible** thanks to material-ui.
* New channel creation   
  It is possible to access the *"Create a channel"* section by clicking the *"Create a channel"* icon on the welcome screen (which is accessible by clicking the *home* button). There, you will be able to specify a channel name and create it.
* Channel membership and access   
  We implemented channel membership - A new user only has access to channel 1 & 2. It is however possible to add a user to a channel with the *"invite friend"* section. (**For demo purpose, it is possible to invite any user registered to any channel created, like this you can invite yourself to Channel 3**).
* Resource access control   
  We *partially* implemented resource access control - **GET functions** require a header including the access token of the user to be successful
* Invite users to channels   
  It is possible to access the *"invite friends"* section by clicking the *"invite friends"* icon on the welcome screen (which is accessible by clicking the *home* button).The whole front-end is done with two nice dynamic combo box that display all the channels created and all the users who connected once on the web app so that the user can choose who to invite and where. If you **add yourself** to a channel, it is required to **REFRESH** the page to have the channel showing in the drawer.
* Message modification   
  Is is possible to **edit** a message only **if you are the author** of this message by clicking the three *vertical dots* next to a message you wrote (which are **not displayed** if you are **not the author**).
* Message removal   
  Is is possible to **remove** a message only **if you are the author** of this message by clicking the *thrash bin* next to a message you wrote (which is **not displayed** if you are **not the author**).
* Account settings   
  It is possible to access *"settings"* by clicking the *"settings"* icon on the welcome screen (which is accessible by clicking the *home* button). Once in the settings it is possible to switch theme between **dark** and **light** on the whole app.
* Gravatar integration   
  Gravatar integration is implemented, once you are logged in it is **always displayed** at the **top right** of the application between your *name* and the *logout icon*.
* Avatar selection   
  It is not possible to select an avatar on the app, you can however *change your gravatar* and the change will be displayed on the web app.
* Personal custom avatar   
  It is not possible to select an avatar on the app, you can however *change your gravatar* and the change will be displayed on the web app.

## Bonus

We did our best to try and make the application as **user-friendly** and **good-looking** as possible.
Both our *light* and *dark* themes are created with care to provide **feedback** to the user (if you hover an *interactive element* on the app such as an icon the background color will change accordingly).

