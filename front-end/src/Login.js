/** @jsx jsx */
import { jsx } from '@emotion/core'
import { useContext, useEffect } from 'react';
import { useCookies } from 'react-cookie';
import crypto from 'crypto'
import qs from 'qs'
import axios from 'axios'
import { useTheme } from '@material-ui/core/styles';
import Link from '@material-ui/core/Link'
import Button from "@material-ui/core/Button"
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import Box from '@material-ui/core/Box';
import Context from './Context'
import {
  useHistory
} from "react-router-dom";

const base64URLEncode = (str) => {
  return str.toString('base64')
    .replace(/\+/g, '-')
    .replace(/\//g, '_')
    .replace(/=/g, '');
}

const sha256 = (buffer) => {
  return crypto
    .createHash('sha256')
    .update(buffer)
    .digest()
}

const useStyles = (theme) => ({
  root: {
    flex: '1 1 auto',
    display: 'flex',
    color: theme.palette.main,
    flexDirection: 'column',
    justifyContent: 'center',
    '& > div': {
      margin: `${theme.spacing(1)}`,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
    '& fieldset': {
      border: 'none',
      '& label': {
        marginBottom: theme.spacing(.5),
        display: 'block',
      },
    },
  },
  form: {
    margin:'auto',
    paddingBottom:'30px',
    paddingTop:'30px',
  },
  container: {
    backgroundColor: theme.palette.background.paper,
    width:'380px',
    height:'300px',
    textAlign:'center',
  },
  field:{
    paddingLeft:'100px',
    paddingTop:'20px',
  }
})

const Redirect = ({
  config,
  codeVerifier,
}) => {
  const styles = useStyles(useTheme())
  const redirect = (e) => {
    e.stopPropagation()
    const code_challenge = base64URLEncode(sha256(codeVerifier))
    const url = [
      `${config.authorization_endpoint}?`,
      `client_id=${config.client_id}&`,
      `scope=${config.scope}&`,
      `response_type=code&`,
      `redirect_uri=${config.redirect_uri}&`,
      `code_challenge=${code_challenge}&`,
      `code_challenge_method=S256`,
    ].join('')
    window.location = url
  }
  return (
    <div css={styles.root}>
      {
        <Box boxShadow={3} style={styles.container}>
          <div style={styles.form}>
            <h1>Welcome !</h1>
            <h2>Connect with Github!</h2>
            <Button
              style={{marginTop:'40px'}}
              variant="contained"
              color="primary"
              size="large"
              css={styles.send}
              endIcon={<NavigateNextIcon />}
              onClick={redirect}
            >
              Login
            </Button>
          </div>
        </Box>
      }
    </div>
  )
}

const Tokens = ({
  oauth
}) => {
  const {setOauth} = useContext(Context)
  const styles = useStyles(useTheme())
  const {id_token} = oauth
  const id_payload = id_token.split('.')[1]
  const {email} = JSON.parse(atob(id_payload))
  const logout = (e) => {
    e.stopPropagation()
    setOauth(null)
  }
  return (
    <div css={styles.root}>
      Welcome {email} <Link onClick={logout} color="secondary">logout</Link>
    </div>
  )
}

export default ({
  onUser
}) => {
  const styles = useStyles(useTheme())
  const history = useHistory();
  // const location = useLocation();
  const [cookies, setCookie, removeCookie] = useCookies([]);
  const {oauth, setOauth} = useContext(Context)
  const config = {
    authorization_endpoint: 'http://127.0.0.1:5556/dex/auth',
    token_endpoint: 'http://127.0.0.1:5556/dex/token',
    client_id: 'webtech-frontend',
    redirect_uri: 'http://127.0.0.1:3000',
    scope: 'openid%20email%20offline_access%20profile&',
  }
  const params = new URLSearchParams(window.location.search)
  const code = params.get('code')
  // is there a code query parameters in the url 
  if(!code){ // no: we are not being redirected from an oauth server
    if(!oauth){
      const codeVerifier = base64URLEncode(crypto.randomBytes(32))
      setCookie('code_verifier', codeVerifier, { path: '/', sameSite: true, secure: false })
      return (
        <Redirect codeVerifier={codeVerifier} config={config} css={styles.root} />
      )
    }else{ // yes: user is already logged in, great, is is working
      return (
        <Tokens oauth={oauth} css={styles.root} />
      )
    }
  }else{ // yes: we are coming from an oauth server
    const codeVerifier = cookies.code_verifier
    useEffect( () => {
      const fetch = async () => {
        try {
          const {data} = await axios.post(
            config.token_endpoint
          , qs.stringify ({
            grant_type: 'authorization_code',
            client_id: `${config.client_id}`,
            code_verifier: `${codeVerifier}`,
            redirect_uri: `${config.redirect_uri}`,
            code: `${code}`,
          }))
          removeCookie('code_verifier')
          setOauth(data)
          const createUser = async () => {
            try {
              await axios.post("http://127.0.0.1:3001/users",
              {
                username : JSON.parse(
                  Buffer.from(
                    data.id_token.split('.')[1], 'base64'
                  ).toString('utf-8')
                ).name,
                channels : ["Channel 1", "Channel 2"]
              }
            )
          }
            catch (err) {
              console.error(err)
            }
          }
          createUser()
          history.push('/')
        }catch (err) {
          console.error(err)
        }
      }
      fetch()
    })
    return (
      <div css={styles.root}>
      {
        <Box boxShadow={3} style={styles.container}>
          <div style={styles.form}>
            <h1>Welcome !</h1>
            <h3>Loading token please wait...</h3>
          </div>
        </Box>
      }
    </div>
    )
  }
}
