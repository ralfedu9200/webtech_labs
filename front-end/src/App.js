import { useContext } from 'react'
/** @jsx jsx */
import { jsx } from '@emotion/core'
// Local
import Oups from './Oups'
import Footer from './Footer'
import Main from './Main'
import Login from './Login'
import Context from './Context'
import { ThemeProvider } from '@material-ui/core/styles';
// Rooter
import {
  Switch,
  Route,
  Redirect,
  useLocation
} from "react-router-dom"

export default () => {
  const location = useLocation()
  const {oauth, theme} = useContext(Context)
  return (
    <div className="App" css={{
      boxSizing: 'border-box',
      display: 'flex',
      flexDirection: 'column',
      backgroundColor:theme.palette.background.default,
    }}>
      <ThemeProvider theme={theme}>
        <Switch>
          <Route exact path="/">
            {
              oauth ? (
                <Redirect
                  to={{
                    pathname: "/channels",
                    state: { from: location }
                  }}
                />
              ) : (
                <Login />
              )
            }
          </Route>
          <Route path="/channels">
            {
              oauth ? (
                <Main />
              ) : (
                <Redirect
                  to={{
                    pathname: "/",
                    state: { from: location }
                  }}
                />
              )
            }
          </Route>
          <Route path="/channels/:id">
            {
              oauth ? (
                <Main />
              ) : (
                <Redirect
                  to={{
                    pathname: "/",
                    state: { from: location }
                  }}
                />
              )
            }
          </Route>
          <Route path="/Oups">
            <Oups />
          </Route>
        </Switch>
        <Footer />
      </ThemeProvider>
    </div>
  );
}
