// *https://www.registers.service.gov.uk/registers/country/use-the-api*
import React from 'react';
import { useContext, useEffect } from 'react'
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useTheme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import SendIcon from '@material-ui/icons/Send';
import Context from './Context'
import axios from 'axios';

const useStyles = (theme) => ({
  root: {
    flex: '1 1 auto',
    display: 'flex',
    height:'100%',
    flexDirection: 'column',
    justifyContent: 'center',
    '& > div': {
      margin: `${theme.spacing(1)}`,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
    '& fieldset': {
      border: 'none',
      '& label': {
        marginBottom: theme.spacing(.5),
        display: 'block',
      },
    },
  },
  form: {
    margin:'auto',
    paddingBottom:'30px',
    paddingTop:'30px',
  },
  container: {
    position: 'relative',
    marginLeft:'auto',
    marginRight:'auto',
    backgroundColor: theme.palette.background.paper,
    width:'450px',
    height:'380px',
    textAlign:'center',
  },
  field:{
    paddingLeft:'100px',
    paddingTop:'20px',
  },
  content: {
    flex: '1 1 auto',
    '&.MuiTextField-root': {
      marginRight: theme.spacing(1),
    },
  },
  send: {
    height:"55px",
    marginTop:'35px',
  },
})

export default function Asynchronous() {
  const classes = useStyles(useTheme())
  const {oauth} = useContext(Context)
  const [openChannels, setOpenChannels] = React.useState(false);
  const [openUsers, setOpenUsers] = React.useState(false);
  const [user, setUser] = React.useState('');
  const [channel, setChannel] = React.useState('');
  const [channelOptions, setChannelOptions] = React.useState([]);
  const [userOptions, setUserOptions] = React.useState([]);
  const loadingChannels = openChannels && channelOptions.length === 0;
  const loadingUsers = openUsers && userOptions.length === 0;
  // Invite a friend
  const onSubmit = async () => {
    try {
      const {data: userInfo} = await axios.get(`http://127.0.0.1:3001/users/${user}`, {
        headers: {
          'Authorization': `Bearer ${oauth.access_token}`
        }
      })
      let channels = userInfo.channels // Get the list of channel names of the user
      channels.push(channel) // Add the channel name to which he is invited
      channels = [...new Set(channels)];
      try {
        // Update the user with the new channel access
        await axios.put(`http://127.0.0.1:3001/users/${user}`, {
          headers: {
            'Authorization': `Bearer ${oauth.access_token}`
          },
          username: user,
          channels : channels,
        })
        console.log(channels)
      }catch (err) {
        console.error(err)
      }
    }catch (err) {
      console.error(err)
    }
  }
  // Use Effect for loading channel list
  useEffect(() => {
    let activeChannel = true;
    if (!loadingChannels) {
      return undefined;
    }
    (async () => {
      const {data: channels} = await axios.get('http://127.0.0.1:3001/channels', {
        headers: {
          'Authorization': `Bearer ${oauth.access_token}`
        }
      })
      if (activeChannel) {
        setChannelOptions(Object.keys(channels).map((key) => channels[key]));
      }
    })();
    return () => {
      activeChannel = false;
    };
  }, [loadingChannels, oauth]);
  // Use Effect for loading user list
  useEffect(() => {
    let activeUsers = true;
    if (!loadingUsers) {
      return undefined;
    }
    (async () => {
      const {data: users} = await axios.get('http://127.0.0.1:3001/users', {
        headers: {
          'Authorization': `Bearer ${oauth.access_token}`
        }
      })
      if (activeUsers) {
        setUserOptions(Object.keys(users).map((key) => users[key]));
      }
    })();
    return () => {
      activeUsers = false;
    };
  }, [loadingUsers, oauth]);
  // Use Effects for managing opening and closing comboboxes
  useEffect(() => {
    if (!openChannels) {
      setChannelOptions([]);
    }
  }, [openChannels]);
  useEffect(() => {
    if (!openUsers) {
      setUserOptions([]);
    }
  }, [openUsers]);
  // Sets the hooks to update the value of the selected user and channel on change
  const handleUserChange = (e) => {
    setUser(e.target.textContent)
  }
  const handleChannelChange = (e) => {
    setChannel(e.target.textContent)
  }
  return (
    <div style={classes.root}>
      
      <Box boxShadow={3} style={classes.container}>
        <h1 style={{marginTop:40}}>Invite a friend to a channel</h1>
        <form style={classes.form} noValidate>
          <Autocomplete
              id="choose-channel"
              open={openChannels}
              style={{margin : '0px 60px 20px 60px'}}
              onOpen={() => {
                setOpenChannels(true);
              }}
              onClose={() => {
                setOpenChannels(false);
              }}
              getOptionSelected={(option, value) => option.name === value.name}
              getOptionLabel={(option) => option.name ? option.name : ""}
              options={channelOptions}
              loading={loadingChannels}
              onChange={handleChannelChange}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Choose a channel"
                  variant="outlined"
                  InputProps={{
                    ...params.InputProps,
                    endAdornment: (
                      <React.Fragment>
                        {loadingChannels ? <CircularProgress color="inherit" size={20} /> : null}
                        {params.InputProps.endAdornment}
                      </React.Fragment>
                    ),
                  }}
                />
              )}
            />
            <Autocomplete
              id="choose-friend"
              open={openUsers}
              style={{margin : '0px 60px 20px 60px'}}
              onOpen={() => {
                setOpenUsers(true);
              }}
              onClose={() => {
                setOpenUsers(false);
              }}
              getOptionSelected={(option, value) => option.username === value.username}
              getOptionLabel={(option) => option.username ? option.username : ""}
              options={userOptions}
              loading={loadingUsers}
              onChange={handleUserChange}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Invite a friend"
                  variant="outlined"
                  InputProps={{
                    ...params.InputProps,
                    endAdornment: (
                      <React.Fragment>
                        {loadingUsers ? <CircularProgress color="inherit" size={20} /> : null}
                        {params.InputProps.endAdornment}
                      </React.Fragment>
                    ),
                  }}
                />
              )}
            />
            <div style={classes.send}>
              <Button
                variant="contained"
                color="primary"
                size="large"
                endIcon={<SendIcon />}
                onClick={onSubmit}
              >
                Invite
              </Button>
            </div>
          </form>
      </Box>
    </div>
  );
}
