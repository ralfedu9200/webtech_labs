import { useState, useContext } from 'react'
/** @jsx jsx */
import { jsx } from '@emotion/core'
// Layout
import Box from '@material-ui/core/Box';
import { useTheme } from '@material-ui/core/styles';
import Context from './Context'
import Switch from '@material-ui/core/Switch';
import { createMuiTheme } from '@material-ui/core/styles';

const useStyles = (theme) => ({
  root: {
    position:'relative',
    height:'100%',
    flexDirection: 'column',
    justifyContent: 'center',
    '& > div': {
      margin: `${theme.spacing(1)}`,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
    '& fieldset': {
      border: 'none',
      '& label': {
        marginBottom: theme.spacing(.5),
        display: 'block',
      },
    },
  },
  form: {
    margin:'auto',
    paddingBottom:'30px',
    paddingTop:'30px',
  },
  container: {
    position: 'relative',
    top:'20%',
    backgroundColor: theme.palette.background.paper,
    width:'380px',
    height:'200px',
    textAlign:'center',
  },
  field:{
    paddingLeft:'100px',
    paddingTop:'20px',
  },
  content: {
    flex: '1 1 auto',
    '&.MuiTextField-root': {
      marginRight: theme.spacing(1),
    },
  },
  send: {
    height:"55px",
    marginTop:'15px',
  },
})

export const darkTheme = createMuiTheme({
  palette: {
    type: 'dark',
    main: 'white',
    link: 'royalBlue',
  }
});

export const lightTheme = createMuiTheme({
  palette: {
    type: 'light',
    main: '#444',
    link: 'royalBlue',
  }
});

export default () => {
  const { theme, setTheme } = useContext(Context)
  const styles = useStyles(useTheme())
  const [darkThemeSwitch, setDarkThemeSwitch] = useState(theme.palette.main === "#444" ? false : true )
  //Handle swith
  const handleChange = () => {
    if(!darkThemeSwitch)
    {
      setTheme(darkTheme)
    }
    else setTheme(lightTheme)
    setDarkThemeSwitch(!darkThemeSwitch);
  };
  return (
    <div css={styles.root}>
    {
      <Box boxShadow={3} style={styles.container}>
        <div style={styles.form}>
          <h1>Settings !</h1>
          Dark theme
          <Switch
            checked={darkThemeSwitch}
            onChange={handleChange}
            name="checked"
            inputProps={{ 'aria-label': 'royalblue' }}
          />
        </div>
      </Box>
    }
    </div>
  )
}