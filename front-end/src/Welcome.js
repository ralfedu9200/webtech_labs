import {} from 'react';
/** @jsx jsx */
import { jsx } from '@emotion/core'
// Layout
import { useTheme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {ReactComponent as ChannelIcon} from './icons/channel.svg';
import {ReactComponent as FriendsIcon} from './icons/friends.svg';
import {ReactComponent as SettingsIcon} from './icons/settings.svg';
import {
  useHistory,
} from 'react-router-dom'

const useStyles = (theme) => ({
  root: {
    height: '100%',
    flex: '1 1 auto',
    display: 'flex',
  },
  card: {
    textAlign: 'center',
    paddingBottom:'30px',
    paddingTop:'30px',
    ':hover':{
      'cursor' : 'pointer',
      'backgroundColor' : theme.palette.background.paper,
    },
  },
  icon: {
    fill: theme.palette.main,
  },
  Iconlabel: {
    color: theme.palette.main,
  }
})

export default () => {
  const styles = useStyles(useTheme())
  const history = useHistory();
  return (
    <div css={styles.root}>
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
        spacing={0}
      >
        <Grid item xs>
          <div css={styles.card} onClick={ (e) => {
              e.preventDefault()
              history.push(`/channels/new`)
            }}>
            <ChannelIcon css={styles.icon} />
            <Typography css={styles.Iconlabel} >
              Create channels
            </Typography>
          </div>
        </Grid>
        <Grid item xs>
          <div css={styles.card} onClick={ (e) => {
              e.preventDefault()
              history.push(`/channels/inviteFriends`)
            }}>
            <FriendsIcon css={styles.icon} />
            <Typography css={styles.Iconlabel} >
              Invite Friends
            </Typography>
          </div>
        </Grid>
        <Grid item xs>
          <div css={styles.card} onClick={ (e) => {
              e.preventDefault()
              history.push(`/channels/settings`)
            }}>
            <SettingsIcon css={styles.icon} />
            <Typography css={styles.Iconlabel} >
              Settings
            </Typography>
          </div>
        </Grid>
      </Grid>
    </div>
  );
}
