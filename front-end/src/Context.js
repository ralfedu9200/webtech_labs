
import React, {useState} from 'react'
import { useCookies } from 'react-cookie'
import {darkTheme} from './Settings'
const Context = React.createContext()

export default Context

export const Provider = ({
  children
}) => {
  const [cookies, setCookie, removeCookie] = useCookies([])
  const [oauth, setOauth] = useState(cookies.oauth)
  const [channels, setChannels] = useState([])
  const [userChannels, setUserChannels] = useState(cookies.userChannels)
  const [currentChannel, setCurrentChannel] = useState(null)
  const [theme, setTheme] = useState(darkTheme)
  return (
    <Context.Provider value={{
      oauth: oauth,
      setOauth: (oauth) => {
        if(oauth){
          const payload = JSON.parse(
            Buffer.from(
              oauth.id_token.split('.')[1], 'base64'
            ).toString('utf-8')
          )
          oauth.email = payload.email
          oauth.name = payload.name
          setCookie('oauth', oauth)
        }else{
          setCurrentChannel(null)
          setChannels([])
          setUserChannels([])
          removeCookie('oauth')
        }
        setOauth(oauth)
      },
      channels: channels,
      setChannels: setChannels,
      currentChannel: currentChannel,
      setCurrentChannel: (channelId) => {
        const channel = channels.find( channel => channel.id === channelId)
        setCurrentChannel(channel)
      },
      theme: theme,
      setTheme: setTheme,
      userChannels : userChannels,
      setUserChannels : setUserChannels,
    }}>{children}</Context.Provider>
  )
}
