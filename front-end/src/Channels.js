/** @jsx jsx */
import {useContext, useEffect} from 'react';
import axios from 'axios';
import { jsx } from '@emotion/core'
import Link from '@material-ui/core/Link'
import Context from './Context'
import { useCookies } from 'react-cookie';
import {useHistory} from 'react-router-dom'
import { useTheme } from '@material-ui/core/styles';

const useStyles = (theme) => ({
  root: {
      height:'100%',
      '& ul': {
          width:'100%',
          paddingLeft:0,
          marginLeft:0,
      },
      '& li': {
          ':hover': {
              backgroundColor:theme.palette.action.hover,
              cursor: 'pointer',
              borderRadius : '10px',
          }
      },
      textAlign: 'center',
  },
  channelIcons: {
      width:'35px',
      height:'35px',
      borderRadius:'50px',
      verticalAlign:"middle"
  },
  channel: {
      flex: '1 1 auto',
      display: 'flex',
      flexDirection: 'column',
      overflow: 'hidden',
  },
  links:{
    color:theme.palette.main,
    textDecoration:'none',
  },
})

export default () => {
  const {
    oauth,
    channels, setChannels,
    userChannels,
  } = useContext(Context)
  const history = useHistory();
  const styles = useStyles(useTheme())
  const [, setCookie, ] = useCookies([]);
  useEffect( () => {
    const fetch = async () => {
      try{
        await axios.get("http://127.0.0.1:3001/users/"+oauth.name, {
          headers: {
            'Authorization': `Bearer ${oauth.access_token}`
          }
        })
        .then((resp)=> (setCookie('userChannels', resp.data.channels, { path: '/', sameSite: true, secure: false })))
      }catch(err){
        console.error(err)
      }
      try{
        const {data: fetchChannels} = await axios.get('http://localhost:3001/channels', {
          headers: {
            'Authorization': `Bearer ${oauth.access_token}`
          }
        })
        let tmpChannels = []
        fetchChannels.forEach(function(channel, i) 
        {
          if(userChannels.includes(channel.name))
          {
            tmpChannels.push(channel)
          }
        }) 
        setChannels(tmpChannels)
      }catch(err){
        console.error(err)
      }
    }
    fetch()
  }, [oauth, setChannels, setCookie, userChannels])
  function addDefaultSrc(ev){
    ev.target.src = 'Channel 1.jpg'
  }
  return (
    <div css={styles.root}>
      <ul style={styles.root}>
        { channels.map( (channel, i) => (
          <li key={i} css={styles.channel}>
            <Link
              href={`/channels/${channel.id}`}
              style={styles.links}
              onClick={ (e) => {
                e.preventDefault()
                history.push(`/channels/${channel.id}`)
              }}
            >
              <p>
                  <img onError={addDefaultSrc} style={styles.channelIcons} src={channel.name+".jpg"} alt=""/>&emsp;
                  {channel.name}
              </p>
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
}
