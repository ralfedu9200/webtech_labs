/** @jsx jsx */
import { jsx } from '@emotion/core'
import {useState} from 'react';
import { useTheme } from '@material-ui/core/styles'

const useStyles = (theme) => ({
    root: {
      width:"100%",
      textAlign:"center",
      backgroundColor:theme.palette.background.default,
      paddingTop: '5px',
      paddingBottom: '5px',
      display:'block',
      flexShrink: 0,
      color: theme.palette.main,
      '& a':{
        color: theme.palette.link
      }
    },
    hideFooter: {
      height:'30px',
      margin:0,
      padding:0,
    },
    hideFooterArrow:{
      display: 'block',
      height: '30px',
      marginLeft: 'auto',
      marginRight: 'auto',
    }
})

export default ({
    Footer
  }) => {
    const styles = useStyles(useTheme())
    const [isHidden, setIsHidden] = useState(true)
    function toggleIsHidden()
    {
        setIsHidden(!isHidden)
    }
    return (
        <footer className="App-footer" >
            <div style={styles.hideFooter} onClick={() => toggleIsHidden()}>
                <img src="hideArrowFooter.png" css={styles.hideFooterArrow} style={{transform: isHidden? "rotate(0deg)" : "rotate(180deg)"}} alt="Hide Arrow"/>
            </div>
            <div css={styles.root} style={{display: isHidden? "none" : "block"}}>
                <p>Contact :<br/>
                <a href="mailto:support@chatwebtech.com">support@chatwebtech.com</a><br/>
                <a href="tel:+33548765456">+33548765456</a><br/>
                3 Rue Marcel Pagnol<br/>
                92500 Rueil-Malmaison<br/>
                France
                </p>
            </div>
        </footer>
    )
}