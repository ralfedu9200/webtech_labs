import {forwardRef, useImperativeHandle, useLayoutEffect, useRef} from 'react'
/** @jsx jsx */
import { jsx } from '@emotion/core'
// Layout
import { useTheme } from '@material-ui/core/styles';
import MoreVert from '@material-ui/icons/MoreVert';
import DeleteOutlineOutlinedIcon from '@material-ui/icons/DeleteOutlineOutlined';
import Context from '../Context'
import { useContext, useState } from 'react'
// Markdown
import unified from 'unified'
import markdown from 'remark-parse'
import remark2rehype from 'remark-rehype'
import html from 'rehype-stringify'
import MessageTime from './MessageTime.js'
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/Button"
import SendIcon from "@material-ui/icons/Send";
import axios from 'axios';
// Time
import dayjs from 'dayjs'
import calendar from 'dayjs/plugin/calendar'
import updateLocale from 'dayjs/plugin/updateLocale'
dayjs.extend(calendar)
dayjs.extend(updateLocale)
dayjs.updateLocale('en', {
  calendar: {
    sameElse: 'DD/MM/YYYY hh:mm A'
  }
})

const useStyles = (theme) => {
  return {
    root: {
      position: 'relative',
      flex: '1 1 auto',
      overflow: 'auto',
      '& ul': {
        'margin': 0,
        'padding': 0,
        'textIndent': 0,
        'listStyleType': 0,
        'listStyle':"none",
      },
      '& a': {
        'color': 'royalblue',
      },
    },
    message: {
      margin: '.2rem',
        padding: '.2rem',
        paddingTop: '.7rem',
        paddingLeft: "15px",
        ':hover': {
          borderRadius : '10px',
          backgroundColor: theme.palette.background.paper,
        },
    },
    h:{
      paddingLeft:"15px",
    },
    author:{
      color: 'tomato',
    },
    fabWrapper: {
      position: 'absolute',
      right: 0,
      top: 0,
      width: '50px',
    },
    fab: {
      position: 'fixed !important',
      top: 0,
      width: '50px',
    },
    paragraph:{
      lineHeight:'25px',
      fontSize:'1.1em',
    },
    
    form: {
      padding: '.5rem',
      display: 'flex',
    },
    content: {
      flex: '1 1 auto',
      '&.MuiTextField-root': {
        marginRight: theme.spacing(1),
      },
    },
    send: {
      height:"55px",
    },
    edit:{
      display:'inline-block',
    },
    editIcon:{
      marginLeft:'3px',
      marginBottom:'2px',
      verticalAlign: 'middle',
      ':hover':{
        'cursor' : 'pointer',
        'backgroundColor' : theme.palette.action.default,
        'borderRadius' : '50%',
      },
      height:'70%',
    },
  }
}

export default forwardRef(({
  channel,
  messages,
  fetchMessages,
  onScrollDown,
}, ref) => {
  const [msgs, setMsgs] = useState(messages ? messages : [])
  const [content, setContent] = useState('')
  const [creation, setCreation] = useState()
  const styles = useStyles(useTheme())
  const {oauth} = useContext(Context)
  // Expose the `scroll` action
  useImperativeHandle(ref, () => ({
    scroll: scroll
  }));
  const rootEl = useRef(null)
  const scrollEl = useRef(null)
  const scroll = () => {
    scrollEl.current.scrollIntoView()
  }
  // See https://dev.to/n8tb1t/tracking-scroll-position-with-react-hooks-3bbj
  const throttleTimeout = useRef(null) // react-hooks/exhaustive-deps
  useLayoutEffect( () => {
    const rootNode = rootEl.current // react-hooks/exhaustive-deps
    const handleScroll = () => {
      if (throttleTimeout.current === null) {
        throttleTimeout.current = setTimeout(() => {
          throttleTimeout.current = null
          const {scrollTop, offsetHeight, scrollHeight} = rootNode // react-hooks/exhaustive-deps
          onScrollDown(scrollTop + offsetHeight < scrollHeight)
        }, 200)
      }
    }
    handleScroll()
    rootNode.addEventListener('scroll', handleScroll)
    return () => rootNode.removeEventListener('scroll', handleScroll)
  })
  // Delete a message with axios delete
  const deleteMsg = async (messages, msg) => {
    try {
      await axios.delete(`http://127.0.0.1:3001/channels/${channel.id}/messages/${msg.creation}`)
      .then(() => {
        const index = messages.indexOf(msg);
        if (index > -1) {
          setMsgs(messages.splice(index, 1))
        }
      })
    }catch (err) {
      console.error(err)
    }
  }
  // Toggle the edit mode
  function toggleEditMode(messages, message) {
    const index = messages.indexOf(message);
    if (index > -1) {
      messages[index].editMode = !messages[index].editMode
      setMsgs(messages)
    }
  }
  // Sets the hooks to update the data of the modified message on change
  const handleChange = (e) => {
    setContent(e.target.value)
    setCreation(e.target.id)
  }
  // Edit a message with axios put
  const onSubmit = async () => {
    try {
      await axios.put(
        `http://127.0.0.1:3001/channels/${channel.id}/messages/${creation}`
      , {
        content: content,
        author: oauth.name,
      }).then(() => {
        messages.forEach(element => {
          if(element.creation === creation && element.author === oauth.name)
          {
            element.content = content
            element.editMode = false
            setMsgs(messages)
            fetchMessages()
          }
        });
      })
    }catch (err) {
      console.error(err)
    }
  }

  return (
    <div css={styles.root} ref={rootEl}>
      <h1 css={styles.h}>Messages for {channel.name}</h1>
      <ul>
        { messages.map( (message, i) => {
          if(message.editMode !==true) message.editMode=false
          if(message.editMode !==false) message.editMode=true
          const {contents: content} = unified()
          .use(markdown)
          .use(remark2rehype)
          .use(html)
          .processSync(message.content)
          return (
            <li key={i} css={styles.message}>
              <span css={styles.author}>{message.author}</span>
              {' - '}
              <MessageTime creation={message.creation}/>
                <div css={styles.edit}>
                {
                  message.author===oauth.name?
                  <div>
                    <MoreVert css={styles.editIcon} onClick={() => toggleEditMode(messages, message)}/>
                    <DeleteOutlineOutlinedIcon css={styles.editIcon} onClick={() =>deleteMsg(messages, message)}/>
                  </div> : ''
                }
                </div>
                <div>
                  {
                    message.editMode?
                    <div>
                      <form css={styles.form} onSubmit={onSubmit} noValidate>
                        <TextField
                          label="Message"
                          multiline
                          rowsMax={4}
                          id={message.creation}
                          onChange={handleChange}
                          variant="outlined"
                          css={styles.content}
                        />
                        <div>
                          <Button
                            variant="contained"
                            color="primary"
                            size="large"
                            css={styles.send}
                            endIcon={<SendIcon />}
                            onClick={onSubmit}
                          >
                            Send
                          </Button>
                        </div>
                      </form>
                    </div>
                    :
                    <div css={styles.paragraph} dangerouslySetInnerHTML={{__html: content}}>
                    </div>
                  }
                </div>
            </li>
          )
        })}
      </ul>
      <div ref={scrollEl} />
    </div>
  )
})