/** @jsx jsx */
import { jsx } from '@emotion/core'
import { useTheme } from '@material-ui/core/styles'
// The main submodule:
import {useEffect, useState} from 'react';
import { formatDistance, formatRelative, subDays, toDate } from 'date-fns'

const useStyles = (theme) => ({
    date: {
        color:'gold'
    }
})

const MessageTime = ({creation}) => {
    creation = creation/1000
    const [time, setTime] = useState(subDays(new Date(creation), 0))
    const styles = useStyles(useTheme())
    useEffect(() => {
        setInterval(() => {
            setTime(subDays(new Date(creation), 0))
        }, 5000)
    })
    
    if(new Date()-toDate(time) < 8.64e8) // If longer than 10 days ago prints date instead of relative date
    {
        return (
            <span className={styles.date}>{(formatDistance(time, new Date(), {includeSeconds:true, addSuffix: true}))}</span>
        )
    }
    else
    {
        return (
            <span className={styles.date}>{(formatRelative(time, new Date(), {includeSeconds:true, addSuffix: true}))}</span>
        )
    }
    
}       

export default MessageTime;
