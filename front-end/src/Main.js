import {useState, useContext} from 'react'
/** @jsx jsx */
import { jsx } from '@emotion/core'
import Channel from './Channel.js';
import Channels from './Channels.js';
import Welcome from './Welcome';
import { makeStyles } from '@material-ui/core/styles'
import Drawer from '@material-ui/core/Drawer';
import Toolbar from '@material-ui/core/Toolbar';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Tooltip from '@material-ui/core/Tooltip';
import HomeIcon from '@material-ui/icons/Home';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ExitToApp from '@material-ui/icons/ExitToApp';
import clsx from 'clsx';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Box from '@material-ui/core/Box';
import Context from './Context';
import Gravatar from 'react-gravatar'
import CreateChannel from './CreateChannel'
import InviteFriends from './InviteFriends';
import Settings from './Settings';
import {
  useHistory,
  useLocation,
  Redirect,
  Route,
  Switch,
} from 'react-router-dom'

const drawerWidth = 270;

const useStyles = makeStyles((theme) => ({
  root:{
    display: 'flex',
    color: theme.palette.main,
    height:'100%',
    flex: '1 1 auto',
  },
  appBar: {
    backgroundColor:theme.palette.background.paper,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
    color : theme.palette.main
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
    backgroundColor:theme.palette.background.paper,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
    
  },
  content: {
    flex: '1 1 auto',
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
  h:{
    marginRight:"10px",
    color : theme.palette.main
  },
  identity: {
    position:'relative',
    color:theme.palette.main,
    float:'right',
    marginLeft:'auto',
    marginRight:0,
  },
  logOutIcon:{
    marginLeft:'25px',
  },
  home:{
    height:'50px',
    width:'50px',
    marginRight:'auto',
    marginTop:'3px',
  },
  gravatar:{
    marginLeft: '10px',
    verticalAlign:'middle', 
    borderRadius:'50%',
  }
}))

export default () => {
  const location = useLocation()
  const classes = useStyles()
  const history = useHistory();
  const {oauth, setOauth} = useContext(Context)
  const setChannel = useState(null)
  const [open, setOpen] = useState(false);
  const handleDrawerOpen = () => {
    setOpen(true);
  };
  // Handle closing the drawer
  const handleDrawerClose = () => {
    setOpen(false);
  };
  // Fetch a channel and sets the channel hook
  const fetchChannel = async (channel) => {
    setChannel(channel)
  }
  // Logout function
  function logout()
  {
    setOauth(null)
  }
  return (
    <main className={classes.root}>
      <Box bgcolor="" className={classes.root}>
        <CssBaseline />
        <AppBar
          position="fixed"
          className={clsx(classes.appBar, {
            [classes.appBarShift]: open,
          })}
        >
        <Toolbar className={classes.toolbar}>
          <Tooltip title="Channel List" aria-label="Channel List">
            <IconButton
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
              className={clsx(classes.menuButton, open && classes.hide)}
            >
              <MenuIcon />
            </IconButton>
          </Tooltip>
          <Tooltip title="Home" aria-label="Home">
            <IconButton
              edge="start"
              onClick={ (e) => {
                e.preventDefault()
                history.push(`/channels`)
              }}
              className={classes.menuButton}
            >
              <HomeIcon />
            </IconButton>
          </Tooltip>
          <div className={classes.identity}>
          {
            oauth.name
          }
          <Tooltip title="Go to gravatar.com" aria-label="Go to gravatar.com">
          {
            <Gravatar onClick={() => (window.location='https://fr.gravatar.com/')} size={40} className={classes.gravatar} email={oauth.email} />
          }
          </Tooltip>
          </div>
          <div className={classes.logOutIcon}>
          <Tooltip title="Log out" aria-label="Log out">
            <IconButton
              edge="start"
              onClick={logout}
              className={classes.menuButton}
            >
              <ExitToApp />
            </IconButton>
            </Tooltip>
          </div>
          </Toolbar>
        </AppBar>
        <Drawer
          className={classes.drawer}
          variant="persistent"
          anchor="left"
          open={open}
          classes={{
            paper: classes.drawerPaper,
          }}
        >
          <div className={classes.drawerHeader}>
            <h2 className={classes.h}>List of channels</h2>
            <IconButton onClick={handleDrawerClose} >
              <ChevronLeftIcon className={classes.menuButton}/>
            </IconButton>
          </div>
          <Divider/>
          <Channels onChannel={fetchChannel} />
        </Drawer>
        <main
          className={clsx(classes.content, {
            [classes.contentShift]: open,
          })}
        >
          <Switch>
            <Route path="/channels/new">
            {
                oauth ? (
                  <CreateChannel addChannel={fetchChannel} />
                ) : (
                  <Redirect
                    to={{
                      pathname: "/",
                      state: { from: location }
                    }}
                  />
                )
              }
            </Route>
            <Route path="/channels/inviteFriends">
            {
                oauth ? (
                  <InviteFriends />
                ) : (
                  <Redirect
                    to={{
                      pathname: "/",
                      state: { from: location }
                    }}
                  />
                )
              }
            </Route>
            <Route path="/channels/settings">
            {
                oauth ? (
                  <Settings />
                ) : (
                  <Redirect
                    to={{
                      pathname: "/",
                      state: { from: location }
                    }}
                  />
                )
              }
            </Route>
            <Route path="/channels/:id">
              <Channel />
            </Route>
            <Route path="/">
              <Welcome />
            </Route>
          </Switch>
        </main>
      </Box>
      
    </main>
  );
}
