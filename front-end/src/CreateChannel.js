import { useState, useContext } from 'react'
import axios from 'axios';
/** @jsx jsx */
import { jsx } from '@emotion/core'
// Layout
import Button from "@material-ui/core/Button"
import SendIcon from "@material-ui/icons/Send";
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import { useTheme } from '@material-ui/core/styles';
import Context from './Context'

const useStyles = (theme) => ({
  root: {
    position:'relative',
    height:'100%',
    flexDirection: 'column',
    justifyContent: 'center',
    '& > div': {
      margin: `${theme.spacing(1)}`,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
    '& fieldset': {
      border: 'none',
      '& label': {
        marginBottom: theme.spacing(.5),
        display: 'block',
      },
    },
  },
  form: {
    margin:'auto',
    paddingBottom:'30px',
    paddingTop:'30px',
  },
  container: {
    position: 'relative',
    top:'20%',
    backgroundColor: theme.palette.background.paper,
    width:'380px',
    height:'300px',
    textAlign:'center',
  },
  field:{
    paddingLeft:'100px',
    paddingTop:'20px',
  },
  content: {
    flex: '1 1 auto',
    '&.MuiTextField-root': {
      marginRight: theme.spacing(1),
    },
  },
  send: {
    height:"55px",
    marginTop:'15px',
  },
})

export default (
  {
    addChannel,
  }) => {
  const {
    channels, setChannels, 
    oauth,
  } = useContext(Context)
  const [content, setContent] = useState('')
  const styles = useStyles(useTheme())
  const onSubmit = async () => {
    const {data: channel} = await axios.post(
      `http://127.0.0.1:3001/channels`, {
      headers: {
        'Authorization': `Bearer ${oauth.access_token}`
      },
        name: content,
        messages: [],
    })
    setChannels([...channels, channel])
    setContent('')
  }
  const handleChange = (e) => {
    setContent(e.target.value)
  }
  return (
    <div css={styles.root}>
    {
      <Box boxShadow={3} style={styles.container}>
        <div style={styles.form}>
          <h1>Create a channel !</h1>
          <form css={styles.form} onSubmit={onSubmit} noValidate>
            <TextField
              id="outlined-multiline-flexible"
              label="Channel name"
              multiline
              rowsMax={4}
              value={content}
              onChange={handleChange}
              css={styles.content}
            />
            <div css={styles.send}>
              <Button
                variant="contained"
                color="primary"
                size="large"
                endIcon={<SendIcon />}
                onClick={onSubmit}
              >
                Send
              </Button>
            </div>
          </form>
        </div>
      </Box>
    }
    </div>
  )
}